# The project is a simple file conversion tool written with Django.

## Requirements

* Python 2.7
* Django 1.6.5

## How to start

* Download the source codes:

        https://korkholeh@bitbucket.org/korkholeh/file_converter.git
        cd file_converter

* Create and activate the virtual environment:

        virtualenv env_fileconv
        source env_fileconv/bin/activate

* Create the public folder with media and static:

        mkdir -p public/media
        mkdir -p public/static

* Install the requirements:

        pip install -r requirements.txt

* Initialize the database:

        cd fileconv
        ./manage.py syncdb

* Start the development server:

        ./manage.py runserver

* Now open the browser and go to `127.0.0.1:8000`.

## Run the unit tests

    ./manage.py test

## How to use

When you open the main page you can see the list of your uploaded files.
You can use link in the right column for download the converted file.

![](http://i.imgur.com/BMRWgTl.png)

Each user can has own list of files. If user is not authenticated then
files are associated with his `session_id`. You can use toolbar in top right
part of the page for login or logout.

If you want to convert the file - press the green button `Add file and convert`.
The next form can be used for uploading the file and selecting the result format.

![](http://i.imgur.com/SOThR45.png)

Depends the file name or selected format for result you can see a couple of
extra fields. For example, the delimiter or quotechar for csv files, or the item
tag for xml.

![](http://i.imgur.com/fg2TQes.png)

You will be able download the result after pressing the button `Upload and convert`.

Example of the converting csv file to xml:

![](http://i.imgur.com/kZet80J.png)

Also you have the admin interface which can helps to see the complete list of the
uploaded and converted files.

![](http://i.imgur.com/JQzpCOG.png)

