from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

from conv.views import (
    FileConversionListView,
    FileConversionCreateView,
)

urlpatterns = patterns(
    '',
    url(
        r'^logout/$',
        'django.contrib.auth.views.logout',
        {'next_page': '/'},
        name='logout'),
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(
        r'^add-file/$',
        FileConversionCreateView.as_view(), name='conversion-add'),
    url(r'^$', FileConversionListView.as_view(), name='conversion-list'),
    url(r'^admin/', include(admin.site.urls)),
)

if getattr(settings, 'DEBUG', False):
    # urlpatterns += patterns(
    #     'django.contrib.staticfiles.views',
    #     url(r'^static/(?P<path>.*)$', 'serve'),
    # )
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': getattr(settings, 'MEDIA_ROOT', ''),
            'show_indexes': True,
            }),
    )
