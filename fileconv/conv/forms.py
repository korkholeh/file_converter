# coding: utf-8
from django import forms
from .models import FileConversion


class FileCreationCreateForm(forms.ModelForm):
    class Meta:
        model = FileConversion
        fields = [
            'source', 'format_for_convertion',
            'csv_delimiter', 'csv_quotechar',
            'xml_row_tag']
