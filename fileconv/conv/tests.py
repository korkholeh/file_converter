import tempfile
from django.test import TestCase
from .models import FileConversion


class ConversionTestCase(TestCase):
    def setUp(self):
        self.csv_tempfile = tempfile.mkstemp(prefix='.csv')[1]
        self.xml_tempfile = tempfile.mkstemp(prefix='.xml')[1]
        self.json_tempfile = tempfile.mkstemp(prefix='.json')[1]

        self.csv_to_xml = FileConversion.objects.create(
            session='123456',
            source=self.csv_tempfile,
            format_for_convertion='xml',
        )
        self.csv_to_json = FileConversion.objects.create(
            session='123456',
            source=self.csv_tempfile,
            format_for_convertion='json',
        )

        self.xml_to_csv = FileConversion.objects.create(
            session='123456',
            source=self.xml_tempfile,
            format_for_convertion='csv',
        )
        self.xml_to_json = FileConversion.objects.create(
            session='123456',
            source=self.xml_tempfile,
            format_for_convertion='json',
        )

        self.json_to_csv = FileConversion.objects.create(
            session='123456',
            source=self.json_tempfile,
            format_for_convertion='csv',
        )
        self.json_to_xml = FileConversion.objects.create(
            session='123456',
            source=self.json_tempfile,
            format_for_convertion='xml',
        )

    def test_csv_to_xml(self):
        _input = open(self.csv_tempfile, 'w')
        _input.write('A,B\n')
        _input.write('A1,B1\n')
        _input.write('A2,B2\n')
        _input.close()
        _xml_tempfile = tempfile.mkstemp(prefix='.xml')[1]
        self.csv_to_xml.csv_to_xml(self.csv_tempfile, _xml_tempfile)
        _output = open(_xml_tempfile, 'r')
        _xml = _output.read()
        _output.close()

        expected = '''<?xml version="1.0"?>
<csv_data>
<item>
    <A>A1</A>
    <B>B1</B>
</item>
<item>
    <A>A2</A>
    <B>B2</B>
</item>
</csv_data>
'''
        self.assertEqual(_xml, expected)

    def test_csv_to_json(self):
        _input = open(self.csv_tempfile, 'w')
        _input.write('A,B\n')
        _input.write('A1,B1\n')
        _input.write('A2,B2\n')
        _input.close()
        _json_tempfile = tempfile.mkstemp(prefix='.json')[1]
        self.csv_to_json.csv_to_json(self.csv_tempfile, _json_tempfile)
        _output = open(_json_tempfile, 'r')
        _json = _output.read()
        _output.close()

        expected = '[{"A": "A1", "B": "B1"}, {"A": "A2", "B": "B2"}]'
        self.assertEqual(_json, expected)

    def test_xml_to_csv(self):
        _input = open(self.xml_tempfile, 'w')
        _input.write('''<?xml version="1.0"?>
<csv_data>
<item>
    <A>A1</A>
    <B>B1</B>
</item>
<item>
    <A>A2</A>
    <B>B2</B>
</item>
</csv_data>
''')
        _input.close()
        _csv_tempfile = tempfile.mkstemp(prefix='.csv')[1]
        self.xml_to_csv.xml_to_csv(self.xml_tempfile, _csv_tempfile)
        _output = open(_csv_tempfile, 'r')
        _csv = _output.read()
        _output.close()

        expected = '''A,B
"A1","B1"
"A2","B2"
'''
        self.assertEqual(_csv, expected)

    def test_xml_to_json(self):
        _input = open(self.xml_tempfile, 'w')
        _input.write('''<?xml version="1.0"?>
<csv_data>
<item>
    <A>A1</A>
    <B>B1</B>
</item>
<item>
    <A>A2</A>
    <B>B2</B>
</item>
</csv_data>
''')
        _input.close()
        _json_tempfile = tempfile.mkstemp(prefix='.json')[1]
        self.xml_to_json.xml_to_json(self.xml_tempfile, _json_tempfile)
        _output = open(_json_tempfile, 'r')
        _json = _output.read()
        _output.close()

        expected = '''{
    "csv_data": {
        "item": [
            {
                "A": "A1", 
                "B": "B1"
            }, 
            {
                "A": "A2", 
                "B": "B2"
            }
        ]
    }
}'''
        self.assertEqual(_json, expected)

    def test_json_to_csv(self):
        _input = open(self.json_tempfile, 'w')
        _input.write('[{"A": "A1", "B": "B1"}, {"A": "A2", "B": "B2"}]')
        _input.close()
        _csv_tempfile = tempfile.mkstemp(prefix='.csv')[1]
        self.json_to_csv.json_to_csv(self.json_tempfile, _csv_tempfile)
        _output = open(_csv_tempfile, 'r')
        _csv = _output.read()
        _output.close()

        expected = '''"A","B"
"A1","B1"
"A2","B2"
'''
        self.assertEqual(_csv, expected)

    def test_json_to_xml(self):
        _input = open(self.json_tempfile, 'w')
        _input.write('[{"A": "A1", "B": "B1"}, {"A": "A2", "B": "B2"}]')
        _input.close()
        _xml_tempfile = tempfile.mkstemp(prefix='.xml')[1]
        self.json_to_xml.json_to_xml(self.json_tempfile, _xml_tempfile)
        _output = open(_xml_tempfile, 'r')
        _xml = _output.read()
        _output.close()

        expected = '<?xml version="1.0" encoding="UTF-8" ?><root><item>' \
            '<A>A1</A><B>B1</B></item><item><A>A2</A><B>B2</B></item></root>'
        # import pdb; pdb.set_trace()
        self.assertEqual(_xml, expected)
