# coding: utf-8
from django.core.urlresolvers import reverse
from django.views.generic.list import ListView
from django.views.generic import CreateView
from .models import FileConversion
from .forms import FileCreationCreateForm


class FileConversionListView(ListView):
    model = FileConversion

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated():
            return FileConversion.objects.filter(
                user=user)
        else:
            session = self.request.session.session_key
            return FileConversion.objects.filter(session=session)


class FileConversionCreateView(CreateView):
    model = FileConversion
    form_class = FileCreationCreateForm

    def form_valid(self, form):
        user = self.request.user
        file_conversion = form.save(commit=False)
        if user.is_authenticated():
            file_conversion.user = user
        else:
            file_conversion.session = self.request.session.session_key
        file_conversion.save()
        file_conversion.do_convertion()
        return super(FileConversionCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('conversion-list')
