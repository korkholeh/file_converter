# coding: utf-8
from django.contrib import admin
from .models import FileConversion


class FileConversionAdmin(admin.ModelAdmin):
    list_display = [
        'user', 'session', 'source',
        'format_for_convertion', 'converted_file', 'modified']
    actions = ['action_convert']

    def action_convert(self, request, queryset):
        for item in queryset:
            item.do_convertion()
    action_convert.short_description = u'Convert files'

admin.site.register(FileConversion, FileConversionAdmin)
