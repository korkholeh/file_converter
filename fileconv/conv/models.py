# coding: utf-8
import os
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


FILE_FORMATS = (
    ('csv', 'CSV'),
    ('json', 'JSON'),
    ('xml', 'XML'),
)


class FileConversion(models.Model):
    """The main class which use for storing the data about file
    conversion operation.

    **Attributes**

    user
       The user to which the convertion operation belongs to

    session
       The session to which the convertion operation belongs to
       if user is not authorized
    """
    user = models.ForeignKey(
        User, verbose_name=u'User', blank=True, null=True)
    session = models.CharField(
        u'Session', blank=True, max_length=100,
        help_text=u'Use for anonymous users.')
    source = models.FileField(u'Source', upload_to='sources')
    format_for_convertion = models.CharField(
        u'Format for convertion', max_length=10, choices=FILE_FORMATS)

    csv_delimiter = models.CharField(
        u'CSV delimiter', max_length=5, default=',',
        help_text=u'For CSV sources only')
    csv_quotechar = models.CharField(
        u'CSV quotechar', max_length=5, default='"',
        help_text=u'For CSV sources only')

    xml_row_tag = models.CharField(
        u'XML row tag', max_length=100, default='item',
        help_text=u'For XML sources only')

    converted_file = models.FileField(
        u'Converted file', upload_to='converted', blank=True, null=True)
    modified = models.DateTimeField(u'Modified', auto_now=True)

    class Meta:
        verbose_name = 'File convertion'
        verbose_name_plural = 'File conversions'

    def __unicode__(self):
        return u'{0}:{1}:{2}->{3}'.format(
            unicode(self.user), self.session,
            self.source, self.converted_file)

    def get_source_file_name(self):
        return os.path.basename(self.source.name)

    def do_convertion(self):
        CONV_DIR = os.path.join(settings.MEDIA_ROOT, 'converted')

        file_path = self.source.path
        file_name, file_extension = os.path.splitext(file_path)
        _fp, _fn = os.path.split(file_name)
        target = os.path.join(CONV_DIR, _fn) + '.' + self.format_for_convertion

        if not os.path.exists(CONV_DIR):
            os.makedirs(CONV_DIR)
        if file_extension == '.csv':
            if self.format_for_convertion == 'xml':
                self.csv_to_xml(file_path, target)
            elif self.format_for_convertion == 'json':
                self.csv_to_json(file_path, target)
        elif file_extension == '.xml':
            if self.format_for_convertion == 'csv':
                self.xml_to_csv(file_path, target)
            elif self.format_for_convertion == 'json':
                self.xml_to_json(file_path, target)
        elif file_extension == '.json':
            if self.format_for_convertion == 'csv':
                self.json_to_csv(file_path, target)
            elif self.format_for_convertion == 'xml':
                self.json_to_xml(file_path, target)

    def csv_to_xml(self, _from, _to):
        import csv
        import cgi
        csv_data = csv.reader(
            open(_from),
            delimiter=str(self.csv_delimiter),
            quotechar=str(self.csv_quotechar))
        xml_data = open(_to, 'w')
        xml_data.write('<?xml version="1.0"?>' + "\n")
        xml_data.write('<csv_data>' + "\n")

        row_num = 0
        for row in csv_data:
            if row_num == 0:
                tags = row
                # replace spaces w/ underscores in tag names
                for i in range(len(tags)):
                    tags[i] = tags[i].replace(' ', '_')
            else:
                xml_data.write('<' + self.xml_row_tag + '>' + "\n")
                for i in range(len(tags)):
                    xml_data.write(
                        '    ' + '<' + tags[i] + '>' +
                        cgi.escape(row[i]) + '</' + tags[i] + '>' + "\n")
                xml_data.write('</' + self.xml_row_tag + '>' + "\n")

            row_num += 1

        xml_data.write('</csv_data>' + "\n")
        xml_data.close()
        self.converted_file.name = os.path.relpath(_to, settings.MEDIA_ROOT)
        self.save()

    def csv_to_json(self, _from, _to):
        import csv
        import json
        csv_data = csv.reader(
            open(_from),
            delimiter=str(self.csv_delimiter),
            quotechar=str(self.csv_quotechar))
        _data = []

        row_num = 0
        for row in csv_data:
            if row_num == 0:
                tags = row
                # replace spaces w/ underscores in tag names
                for i in range(len(tags)):
                    tags[i] = tags[i].replace(' ', '_')
            else:
                _data_row = {}
                for i in range(len(tags)):
                    _data_row[tags[i]] = row[i]
                _data.append(_data_row)
            row_num += 1

        with open(_to, 'w') as outfile:
            json.dump(_data, outfile, ensure_ascii=False)
        self.converted_file.name = os.path.relpath(_to, settings.MEDIA_ROOT)
        self.save()

    def xml_to_csv(self, _from, _to):
        from xmlutils.xml2csv import xml2csv
        converter = xml2csv(_from, _to, encoding="utf-8")
        converter.convert(tag=self.xml_row_tag)
        self.converted_file.name = os.path.relpath(_to, settings.MEDIA_ROOT)
        self.save()

    def xml_to_json(self, _from, _to):
        from xmlutils.xml2json import xml2json
        converter = xml2json(_from, _to, encoding="utf-8")
        converter.convert()
        self.converted_file.name = os.path.relpath(_to, settings.MEDIA_ROOT)
        self.save()

    def json_to_csv(self, _from, _to):
        import json

        with open(_from, 'r') as infile:
            _data = json.load(infile, encoding='utf-8')
            if len(_data) > 0:
                headers = _data[0].keys()

            with open(_to, 'w') as outfile:
                outfile.write(
                    self.csv_delimiter.join([
                        self.csv_quotechar+h+self.csv_quotechar for h in headers])+'\n')
                for item in _data:
                    outfile.write(
                        self.csv_delimiter.join(
                            [self.csv_quotechar+i+self.csv_quotechar for i in item.values()])+'\n')
            self.converted_file.name = os.path.relpath(
                _to, settings.MEDIA_ROOT)
            self.save()

    def json_to_xml(self, _from, _to):
        import json
        import dicttoxml

        with open(_from, 'r') as infile:
            _data = json.load(infile, encoding='utf-8')

            xml = dicttoxml.dicttoxml(_data, attr_type=False)
            with open(_to, 'w') as outfile:
                outfile.write(xml)
            self.converted_file.name = os.path.relpath(
                _to, settings.MEDIA_ROOT)
            self.save()
